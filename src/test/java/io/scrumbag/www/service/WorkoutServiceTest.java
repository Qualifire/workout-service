package io.scrumbag.www.service;

import io.scrumbag.www.model.Exercise;
import io.scrumbag.www.model.Workout;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.repo.WorkoutRepo;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WorkoutServiceTest extends BaseServiceTest {


    WorkoutRepo workoutRepo = mock(WorkoutRepo.class);
    WorkoutService workoutService;

    @Before
    public void setUp() {
        workoutService = new WorkoutService(workoutRepo);
    }

    @Test
    public void testGetAllWorkouts() {
        when(workoutRepo.findAll())
                .thenReturn(Arrays.asList(
                        new Workout(
                                WorkoutType.WORKOUT_A,
                                Arrays.asList(new Exercise("A", 1, 1), new Exercise("B", 2, 2)),
                                LocalDate.now().minusDays(3),
                                null
                        ),
                        new Workout(
                                WorkoutType.WORKOUT_B,
                                Arrays.asList(new Exercise("C", 3, 3), new Exercise("D", 4, 4)),
                                LocalDate.now(),
                                null
                        )
                        )
                );

        List<Workout> getAllWorkoutsResult = workoutService.findAllWorkouts();
        verify(workoutRepo, only()).findAll();
    }

    @Test
    public void testCreateWorkout() {
        Workout workout = new Workout(
                WorkoutType.WORKOUT_ABS,
                Arrays.asList(new Exercise("G", 3, 3), new Exercise("H", 4, 4)),
                LocalDate.now(),
                null
        );
        workoutService.registerWorkout(workout);

        verify(workoutRepo, only()).save(workout);
    }
}