package io.scrumbag.www.service;

import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.Exercise;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.repo.BluePrintRepo;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BluePrintServiceTest extends BaseServiceTest {

    private BluePrintRepo bluePrintRepo = mock(BluePrintRepo.class);
    private BluePrintService bluePrintService;

    private Blueprint b1, b2;

    @Before
    public void setUp() {

        bluePrintService = new BluePrintService(bluePrintRepo);
        b1 = new Blueprint(
                WorkoutType.WORKOUT_A,
                Arrays.asList(new Exercise("Exercise 1", 5, 5), new Exercise("Exercise 2", 5, 5)),
                1L
        );
        b2 = new Blueprint(
                WorkoutType.WORKOUT_B,
                Arrays.asList(new Exercise("Work", 5, 5), new Exercise("Eat", 5, 5), new Exercise("Sleep", 1, 5)),
                10L
        );
    }

    @Test
    public void testFindAll() throws Exception {
        when(bluePrintRepo.findAll()).thenReturn(
                Arrays.asList(b1, b2)
        );
        List<Blueprint> allBluePrints = bluePrintService.findAll();
        verify(bluePrintRepo,only()).findAll();
        assertThat(allBluePrints.size()).isEqualTo(2);
        assertThat(allBluePrints.get(0).getExercises().size()).isEqualTo(2);
        assertThat(allBluePrints.get(1).getExercises().size()).isEqualTo(3);

    }

    @Test
    public void testFindByWorkoutType() throws Exception {
        when(bluePrintRepo.findByWorkoutType(WorkoutType.WORKOUT_A)).thenReturn(
                b1
        );

        when(bluePrintRepo.findByWorkoutType(WorkoutType.WORKOUT_B)).thenReturn(
                b2
        );
        when(bluePrintRepo.findByWorkoutType(WorkoutType.WORKOUT_ABS)).thenReturn(
                null
        );
        Blueprint bpForWorkoutA = bluePrintService.findByWorkoutType(WorkoutType.WORKOUT_A);
        Blueprint bpForWorkoutB = bluePrintService.findByWorkoutType(WorkoutType.WORKOUT_B);
        Blueprint bpForWorkoutAbs = bluePrintService.findByWorkoutType(WorkoutType.WORKOUT_ABS);
        verify(bluePrintRepo).findByWorkoutType(WorkoutType.WORKOUT_A);
        verify(bluePrintRepo).findByWorkoutType(WorkoutType.WORKOUT_B);
        verify(bluePrintRepo).findByWorkoutType(WorkoutType.WORKOUT_ABS);
    }
}