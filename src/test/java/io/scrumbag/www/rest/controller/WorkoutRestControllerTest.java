package io.scrumbag.www.rest.controller;

import io.scrumbag.www.WorkoutServiceApplication;
import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.Exercise;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.repo.BluePrintRepo;
import io.scrumbag.www.rest.form.WorkoutForm;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WorkoutServiceApplication.class)
@WebAppConfiguration
public class WorkoutRestControllerTest {

    private MediaType mediaType = MediaType.parseMediaType("application/json;charset=UTF-8");
    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private BluePrintRepo bluePrintRepo;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assertions.assertThat(this.mappingJackson2HttpMessageConverter).isNotNull();
    }

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();

        bluePrintRepo.deleteAll();
    }

    @Test
    public void testGetAllWorkoutTypes() throws Exception {
        MvcResult result = mockMvc.perform(get("/workout-types").accept(mediaType))
                .andExpect(content().contentType(mediaType))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("WORKOUT_A")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("WORKOUT_B")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("WORKOUT_ABS")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("CARDIO_LISS")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("CARDIO_HIIT")).isTrue();

    }

    @Test
    public void testGetAllBluePrints() throws Exception {

        prepareTestBluePrintData();


        MvcResult result = mockMvc.perform(get("/blueprints").accept(mediaType))
                .andExpect(content().contentType(mediaType))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("WORKOUT_A")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("WORKOUT_B")).isTrue();
        assertThat(result.getResponse().getContentAsString().contains("{\"name\":\"Work\",\"sets\":5,\"reps\":5},{\"name\":\"Eat\",\"sets\":5,\"reps\":5},{\"name\":\"Sleep\",\"sets\":1,\"reps\":5}")).isTrue();
    }

    @Test
    public void testWorkoutRegistration() throws Exception {
        WorkoutForm form = new WorkoutForm(
                "Stronglifts - Workout A",
                Arrays.asList(new Exercise("Exercise 1", 5, 5), new Exercise("Exercise 2", 5, 5)),
                LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                "blabla"
        );


        String workoutJson = json(form);
        System.out.println(workoutJson);
        this.mockMvc.perform(post("/workouts")
                .contentType(mediaType)
                .content(workoutJson))
                .andExpect(status().isCreated());

    }

    @Test
    public void testWorkoutWithWrongDate() throws Exception {
        WorkoutForm form = new WorkoutForm(
                "Stronglifts - Workout A",
                Arrays.asList(new Exercise("Exercise 1", 5, 5), new Exercise("Exercise 2", 5, 5)),
                "55-1-9011",
                "blabla"
        );


        String workoutJson = json(form);
        MvcResult result = this.mockMvc.perform(post("/workouts")
                .contentType(mediaType)
                .content(workoutJson))
                .andExpect(status().is4xxClientError())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringCase("Invalid date value for field: workout date");

    }

    @Test
    public void testWorkoutWithNoExercises() throws Exception {
        WorkoutForm form = new WorkoutForm(
                "Stronglifts - Workout A",
                null,
                "01-02-1999",
                "blabla"
        );


        String workoutJson = json(form);
        MvcResult result = this.mockMvc.perform(post("/workouts")
                .contentType(mediaType)
                .content(workoutJson))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

    private void prepareTestBluePrintData() {
        Blueprint b1 = new Blueprint(
                WorkoutType.WORKOUT_A,
                Arrays.asList(new Exercise("Exercise 1", 5, 5), new Exercise("Exercise 2", 5, 5)),
                1L
        );
        Blueprint b2 = new Blueprint(
                WorkoutType.WORKOUT_B,
                Arrays.asList(new Exercise("Work", 5, 5), new Exercise("Eat", 5, 5), new Exercise("Sleep", 1, 5)),
                10L
        );

        Stream.of(b1, b2).forEach(bp -> bluePrintRepo.save(bp));
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}