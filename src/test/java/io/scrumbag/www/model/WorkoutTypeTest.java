package io.scrumbag.www.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class WorkoutTypeTest {
    @Test
    public void testFromString() {
        String value1 = "Stronglifts - Workout A";
        String value2 = "Cardio - H.I.I.T.";

        Assertions.assertThat(WorkoutType.WORKOUT_A == WorkoutType.fromString(value1)).isTrue();
        Assertions.assertThat(WorkoutType.CARDIO_HIIT == WorkoutType.fromString(value2)).isTrue();


    }

    @Test(expected = IllegalStateException.class)
    public void testCorrectException() {

        String valueNonExistant = "blabla";
        WorkoutType.fromString(valueNonExistant);

    }

}