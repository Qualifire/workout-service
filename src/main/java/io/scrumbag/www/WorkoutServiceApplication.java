package io.scrumbag.www;

import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.Exercise;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.repo.BluePrintRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import java.util.Arrays;
import java.util.stream.Stream;

@SpringBootApplication
@ImportResource({"classpath:workout-types.xml"})
public class WorkoutServiceApplication {


    @Bean
    CommandLineRunner runner(BluePrintRepo bluePrintRepo) {
        Blueprint b1 = new Blueprint(
                WorkoutType.WORKOUT_A,
                Arrays.asList(new Exercise("Bench Press", 5, 5), new Exercise("Squat", 5, 5), new Exercise("Row", 5, 5)),
                120L
        );
        Blueprint b2 = new Blueprint(
                WorkoutType.WORKOUT_B,
                Arrays.asList(new Exercise("OH Press", 5, 5), new Exercise("Squat", 5, 5), new Exercise("Deadlift", 1, 5)),
                120L
        );
        Blueprint b3 = new Blueprint(
                WorkoutType.WORKOUT_ABS,
                Arrays.asList(
                        new Exercise("Russian Twists", 3, 20),
                        new Exercise("Leg Raise", 3, 20),
                        new Exercise("Crunch", 3, 20),
                        new Exercise("Heel Touches", 3, 20),
                        new Exercise("Modified V-Up", 3, 20)
                ),
                60L
        );

        return args -> Stream.of(b1, b2, b3).forEach(blueprint -> bluePrintRepo.save(blueprint));
    }

    public static void main(String[] args) {
        SpringApplication.run(WorkoutServiceApplication.class, args);
    }
}
