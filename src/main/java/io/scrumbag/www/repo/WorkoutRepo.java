package io.scrumbag.www.repo;

import io.scrumbag.www.model.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface WorkoutRepo extends JpaRepository<Workout, Long> {
    Workout findByWorkoutDate(LocalDate searchDate);
}
