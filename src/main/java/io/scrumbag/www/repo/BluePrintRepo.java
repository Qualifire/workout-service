package io.scrumbag.www.repo;

import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.WorkoutType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BluePrintRepo extends JpaRepository<Blueprint, Long> {

    List<Blueprint> findAll();

    Blueprint findByWorkoutType(WorkoutType workoutType);


}
