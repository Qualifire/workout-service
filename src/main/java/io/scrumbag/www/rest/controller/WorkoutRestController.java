package io.scrumbag.www.rest.controller;

import io.scrumbag.www.exception.EmptyRequestBodyException;
import io.scrumbag.www.exception.InvalidDateException;
import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.Workout;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.rest.form.WorkoutForm;
import io.scrumbag.www.service.BluePrintService;
import io.scrumbag.www.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class WorkoutRestController {

    private BluePrintService bluePrintService;
    private WorkoutService workoutService;

    @Autowired
    public WorkoutRestController(BluePrintService bluePrintService, WorkoutService workoutService) {
        this.bluePrintService = bluePrintService;
        this.workoutService = workoutService;
    }

    @Resource(name = "workout-types")
    private List<String> workoutTypes;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome() {
        return "Hello WorkoutService";
    }

    @RequestMapping(value = "/workout-types", method = RequestMethod.GET)
    public List<WorkoutType> findAllWorkoutTypes() {

        return workoutTypes.stream()
                .map(WorkoutType::fromString)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/blueprints", method = RequestMethod.GET)
    public List<Blueprint> findAllBluePrints() {
        return bluePrintService.findAll();
    }

    @RequestMapping(value = "/workouts", method = RequestMethod.POST)
    public ResponseEntity<?> bookWorkout(@RequestBody @Valid final WorkoutForm workoutForm) {
        notNullRequestBody(workoutForm);
        validateDate("workout date", workoutForm.getWorkoutDate());
        WorkoutType woType = WorkoutType.fromString(workoutForm.getWorkoutType());
        LocalDate registrationDate = LocalDate.parse(workoutForm.getWorkoutDate(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        workoutService.registerWorkout(new Workout(woType, workoutForm.getPerformedExercises(), registrationDate, workoutForm.getRemarks()));

        return new ResponseEntity<>(null, null, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/workouts", method = RequestMethod.GET)
    public List<Workout> findAllWorkouts() {
        return workoutService.findAllWorkouts();
    }

    @RequestMapping(value = "/workouts/{workoutDate}", method = RequestMethod.GET)
    public Workout searchWorkoutForDate(@PathVariable (value = "workoutDate") final String workoutDate){
        return workoutService.findWorkoutForDate(workoutDate);
    }

    private void notNullRequestBody(Object body) {
        if (body == null) {
            throw new EmptyRequestBodyException();
        }
    }

    private void validateDate(String fieldName, String date) {
        try{
            LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }catch (DateTimeParseException e){
            throw new InvalidDateException(fieldName);
        }
    }
}
