package io.scrumbag.www.rest.form;

import io.scrumbag.www.model.Exercise;

import javax.validation.constraints.NotNull;
import java.util.List;

public class WorkoutForm {
    @NotNull
    private String workoutType;

    @NotNull
    private List<Exercise> performedExercises;

    @NotNull
    private String workoutDate;

    private String remarks;

    WorkoutForm() {
    }

    public WorkoutForm(String workoutType, List<Exercise> performedExercises, String workoutDate, String remarks) {
        this.workoutType = workoutType;
        this.performedExercises = performedExercises;
        this.workoutDate = workoutDate;
        this.remarks = remarks;
    }

    public String getWorkoutType() {
        return workoutType;
    }

    public List<Exercise> getPerformedExercises() {
        return performedExercises;
    }

    public String getWorkoutDate() {
        return workoutDate;
    }

    public String getRemarks() {
        return remarks;
    }
}
