package io.scrumbag.www.service;

import io.scrumbag.www.model.Workout;
import io.scrumbag.www.repo.WorkoutRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class WorkoutService {

    private WorkoutRepo workoutRepo;

    @Autowired
    public WorkoutService(final WorkoutRepo workoutRepo) {
        this.workoutRepo = workoutRepo;
    }

    public void registerWorkout(final Workout workout){
        workoutRepo.save(workout);
    }

    public List<Workout> findAllWorkouts() {
        return workoutRepo.findAll();
    }

    public Workout findWorkoutForDate(final String workoutDate) {
        LocalDate searchDate = LocalDate.parse(workoutDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        return workoutRepo.findByWorkoutDate(searchDate);
    }
}
