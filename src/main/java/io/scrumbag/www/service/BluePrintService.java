package io.scrumbag.www.service;

import io.scrumbag.www.model.Blueprint;
import io.scrumbag.www.model.WorkoutType;
import io.scrumbag.www.repo.BluePrintRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BluePrintService {

    private BluePrintRepo bluePrintRepository;

    @Autowired
    public BluePrintService(BluePrintRepo bluePrintRepository) {
        this.bluePrintRepository = bluePrintRepository;
    }

    public List<Blueprint> findAll() {
        return bluePrintRepository.findAll();
    }

    public Blueprint findByWorkoutType(WorkoutType workoutType){
        return bluePrintRepository.findByWorkoutType(workoutType);
    }
}
