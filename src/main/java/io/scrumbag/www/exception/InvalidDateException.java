package io.scrumbag.www.exception;

public class InvalidDateException extends IllegalArgumentException {

    private final String fieldName;

    public InvalidDateException(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
