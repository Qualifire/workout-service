package io.scrumbag.www.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler(EmptyRequestBodyException.class)
    public ResponseEntity<?> handleEmptyRequestBodyException(final EmptyRequestBodyException e) {
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(IllegalAuthorizationException.class)
    public ResponseEntity<?> handleIllegalAuthorizationException(final IllegalAuthorizationException e) {
        return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InvalidDateException.class)
    public ResponseEntity<?> handleInvalidDateException(final InvalidDateException e) {
        return ResponseEntity.badRequest().body("Invalid date value for field: " + e.getFieldName());
    }


}