package io.scrumbag.www.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;
@Entity
public class Workout {
    @Id
    @GeneratedValue
    private Long id;
    @Enumerated(EnumType.STRING)
    private WorkoutType workoutType;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Exercise> performedExercises;
    private LocalDate workoutDate;
    private String remarks;

    Workout() {
    }

    public Workout(WorkoutType workoutType, List<Exercise> performedExercises, LocalDate workoutDate, String remarks) {
        this.workoutType = workoutType;
        this.performedExercises = performedExercises;
        this.workoutDate = workoutDate;
        this.remarks = remarks;
    }

    public Long getId() {
        return id;
    }

    public WorkoutType getWorkoutType() {
        return workoutType;
    }

    public List<Exercise> getPerformedExercises() {
        return performedExercises;
    }

    public LocalDate getWorkoutDate() {
        return workoutDate;
    }

    public String getRemarks() {
        return remarks;
    }
}
