package io.scrumbag.www.model;

import java.util.Arrays;

public enum WorkoutType {
    WORKOUT_A("Stronglifts - Workout A"),
    WORKOUT_B("Stronglifts - Workout B"),
    CARDIO_LISS("Cardio - L.I.S.S."),
    CARDIO_HIIT("Cardio - H.I.I.T."),
    WORKOUT_ABS("Abs");

    private String name;

    WorkoutType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static WorkoutType fromString(String description) {
        if (description != null) {
            return Arrays.stream(WorkoutType.values())
                    .filter(workoutType -> workoutType.getName().equalsIgnoreCase(description))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported workout type %s.", description)));
        }

        return null;
    }
}
