package io.scrumbag.www.model;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Blueprint {
    @Id
    @GeneratedValue
    private String id;
    @Enumerated(EnumType.STRING)
    private WorkoutType workoutType;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Exercise> exercises;
    private Long restBetweenSets;

     Blueprint() {
    }

    public Blueprint(WorkoutType workoutType, List<Exercise> exercises, Long restBetweenSets) {
        this.workoutType = workoutType;
        this.exercises = exercises;
        this.restBetweenSets = restBetweenSets;
    }

    public WorkoutType getWorkoutType() {
        return workoutType;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public Long getRestBetweenSets() {
        return restBetweenSets;
    }
}
