package io.scrumbag.www.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Exercise {
    @Id
    @GeneratedValue
    private String id;
    private String name;
    private Integer sets;
    private Integer reps;

    Exercise() {
    }

    public Exercise(String name, Integer sets, Integer reps) {
        this.name = name;
        this.sets = sets;
        this.reps = reps;
    }

    public String getName() {
        return name;
    }

    public Integer getSets() {
        return sets;
    }

    public Integer getReps() {
        return reps;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "name='" + name + '\'' +
                ", sets=" + sets +
                ", reps=" + reps +
                '}';
    }
}
